#In this script, we simulate the data that we will use during the tutorial
### --- 0. Loading libraries and functions --- ####

### --- Bayesian inference --- ###
library(INLA)
library(splancs)

### --- maps --- ###
library(lattice)
library(fields)
library(plotKML)

### --- spatial objects --- ###
library(raster)
library(rgdal)
library(sp)

### --- colors --- ###
library(viridis)

### --- plot --- ###
library(ggplot2)

### --- packages from github --- ###
library(remotes)
#remotes::install_github("chrisbrunsdon/caricRture") #small_chop
library(caricRture)

### --- function to convert matrix to raster --- ###
rotate <- function(x)(apply(x,2,rev))

matrix_to_raster <- function(m, proj.grid.mat = proj.grid.mat, boundary)
{
  raster(rotate(t(m)), 
         xmn = min(proj.grid.mat$lattice$loc[,1]), 
         xmx = max(proj.grid.mat$lattice$loc[,1]), 
         ymn = min(proj.grid.mat$lattice$loc[,2]), 
         ymx = max(proj.grid.mat$lattice$loc[,2]), 
         crs = proj4string(boundary))   
}

### --- 1. Loading the data --- ####
saudi_bound <- getData('GADM', country='SAU', level = 0)
saudi_bound <- small_chop(saudi_bound)
plot(saudi_bound)
saveRDS(saudi_bound, file = "data/saudi_bound.rds")

### --- 2. Generating points in an area --- ####
N <- 200
set.seed(199)

pts <- spsample(saudi_bound, N, type = "random")
pts <- pts@coords

#Plotting
plot(saudi_bound)
points(pts, col = "orange", pch = 20)



### --- 3. Simulating Gaussian with Matérn covariance structure --- ####
dmat <- as.matrix(dist(pts))
sigma2w <- 2
real_range <- 2
kappa <- sqrt(8)/real_range
nu <- 1

# Matérn correlation
cMatern <- function(h, nu, kappa){
  ifelse(h > 0, besselK(h*kappa, nu) * (h*kappa)^nu/(gamma(nu)*2^(nu - 1)), 1)
}

mcor <- cMatern(dmat, nu, kappa)
mcov <- sigma2w*mcor

R <- chol(mcov)
w <- drop(crossprod(R, rnorm(N)))
data <- data.frame(cbind(pts, w))
colnames(data)

#Kaust coordinates
kaust <- data.frame(matrix(c(39.1105, 22.3126) , ncol = 2))
colnames(kaust) <- c("x", "y")

ggplot() +
  geom_polygon(aes(x = long, y = lat), data = saudi_bound,
               colour = 'white', fill = 'gray', alpha = .5, size = .3) +
  geom_point(data = data, aes(x = x, y = y, size = w, color = w)) +
  scale_color_viridis() +
  geom_point(data = kaust, aes(x = x, y = y), size = 5)



ggplot() +
  geom_line(data = data.frame(x = seq(0, 6, 0.01), 
                              y = cMatern(seq(0, 6, 0.01), nu, kappa)),
            aes(x = x, 
                y = y))

plot(saudi_bound)



### --- 4. Adding some covariates --- ####
### ----- 4.1. Downloading covariates from database worldclim --- ####
bio <- getData('worldclim', var = 'bio', res = 2.5)
bio <- crop(bio, saudi_bound) #Boundaries
bio <- mask(bio, saudi_bound) #Values
bio6 <- bio$bio6/10


### ----- 4.2. Generate a variable using the resolution of the raster --- ####
sim <- bio6
values(sim) <- trunc(values(sim))
sim[sim<0] <- 0
names(sim) <- "sim"
writeRaster(sim, "rasters/sim", overwrite = TRUE)

sim <- raster("rasters/sim")


plot(saudi_bound)
plot(sim, add = TRUE)
plot(saudi_bound, add = TRUE)



### --- 5. Simulating response variable --- ####
data <- cbind(data, sim = extract(sim, pts))
data <- remove_missing(data)
beta1 <- + 0.1
beta0 <- - 0.5

#Linear predictor
predictor <- beta0 + beta1*data$sim + data$w
set.seed(150)
resp <- rbinom(dim(data)[1], size = 1, prob = exp(predictor)/(1 + exp(predictor)))

data <- data.frame(x = data$x, y = data$y, sim = data$sim, resp = resp)

summary(resp)
table(resp)

plot(saudi_bound)
points(data$x, data$y, col = c("red", "blue")[as.factor(data$resp)])
points(kaust)

ggplot() +
  geom_polygon(aes(x = long, y = lat), data = saudi_bound,
               colour = 'white', fill = 'gray', alpha = .5, size = .3) +
  geom_point(data = data.frame(data, predictor), aes(x = x, y = y, size = predictor, color = predictor)) +
  scale_color_viridis() +
  geom_point(data = kaust, aes(x = x, y = y), size = 5)

head(data)

#Saving data removing the gf
saveRDS(data[,-3], file = "data/data.rds")
