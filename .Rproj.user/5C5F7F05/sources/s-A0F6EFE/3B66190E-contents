#In this script, we simulate the data that we will use during the tutorial
### --- 0. Loading libraries and functions --- ####

### --- Bayesian inference --- ###
library(INLA)
library(splancs)

### --- maps --- ###
library(lattice)
library(fields)
library(plotKML)

### --- spatial objects --- ###
library(raster)
library(rgdal)
library(sp)

### --- colors --- ###
library(viridis)

### --- plot --- ###
library(ggplot2)

### --- packages from github --- ###
library(remotes)
#remotes::install_github("chrisbrunsdon/caricRture") #small_chop
library(caricRture)

### --- function to convert matrix to raster --- ###
rotate <- function(x)(apply(x,2,rev))

matrix_to_raster <- function(m, proj.grid.mat = proj.grid.mat, boundary)
{
  raster(rotate(t(m)), 
         xmn = min(proj.grid.mat$lattice$loc[,1]), 
         xmx = max(proj.grid.mat$lattice$loc[,1]), 
         ymn = min(proj.grid.mat$lattice$loc[,2]), 
         ymx = max(proj.grid.mat$lattice$loc[,2]), 
         crs = proj4string(boundary))   
}

### --- 1. Loading the data --- ####
### ----- 1.1. Boundaries of Arabia Saudita --- ####
saudi_bound <- readRDS("data/saudi_bound.rds")
plot(saudi_bound)

### ----- 1.2. dataset --- ####
data <- readRDS("data/data.rds")
head(data)

data_plot <- data
data_plot$resp <- as.factor(data_plot$resp)
png("images/data.png", width = 1000, height = 800, res = 100)
ggplot() +
  geom_polygon(aes(x = long, y = lat), data = saudi_bound,
               colour = 'white', fill = 'gray', alpha = .5, size = .3) +
  geom_point(data = data_plot, aes(x = x, y = y, color = resp)) +
  scale_color_manual(values=c("blue", "red"))
dev.off()

### ----- 1.3. Load the raster --- ####
sim <- raster("rasters/sim")

png("images/covariate_sim.png", width = 800, height = 500, res = 100)
par(mar=c(0,0,0,4))
plot(saudi_bound)
plot(sim, 
     add = TRUE,
     legend.width  = 3, 
     legend.shrink = 0.7)
plot(saudi_bound, add = TRUE)
dev.off()


### --- 2. Basic fitting with INLA --- ####
mod.cov <- inla(resp ~ sim,
                data              = data, 
                family            = "binomial" ,
                control.compute   = list(return.marginals = TRUE), 
                control.predictor = list(compute = TRUE),
                control.fixed     = list(mean.intercept = 0,
                                         prec.intercept = 0.0001),
                control.inla      = list(strategy = "simplified.laplace"))

summary(mod.cov)

1 - inla.pmarginal(0, mod.cov$marginals.fixed$sim)

### --- 3. Defining the mesh --- ####

### --- segment from spatial polygon --- ###
borinla <- inla.sp2segment(saudi_bound)


#meshbuilder()
  pts <- as.matrix(data[,1:2])
### --- mesh --- ###
mesh <- inla.mesh.2d(boundary     = borinla,
                     max.edge     = c(1, 2),
                     min.angle    = c(30, 21),
                     cutoff       = 0.4, 
                     offset       = c(0.6, 3)) 


png("images/mesh.png", width = 1700, height = 1700, res = 150)
  plot(mesh)
  points(pts, pch = 20, col = c("green4", "red4")[as.factor(data$resp)])
  plot(saudi_bound, add = TRUE)
dev.off()

### --- 4. Define spatial model and prepare the data --- ####
### ----- 4.1. Definition of the projector matrix between the mesh and the coordinates of the data --- ####
A.est <- inla.spde.make.A(mesh, loc = cbind(data$x, data$y))
saveRDS(A.est, "rds/A_est.rds")

### ----- 4.2. Definition of the spde --- ####
size <- min(c(diff(range(data$x)), diff(range(data$y))))
range0 <- size / 2 	# ~ default

# - The way of setting these priors for sigma is that we do need to set sigma0 
# and p such that P(sigma > sigma 0) = p. In our example we will set 
# sigma0 = 10 and p = 0.01 and these values are passed to the inla.spde2.matern() 
# function as a vector in the next code. 
#
# - For the practical range parameter the setting is that we have to choose r0 
# and p such that P(r < r0) = p. We can set the PC-prior for 
# the median by setting p = 0.5 and in the next code we consider a prior median 
# equal to the diameter of the data divided by 2.

spde <- inla.spde2.pcmatern(
  # Mesh 
  mesh = mesh, 
  # P(practic.range < range0) = 0.5
  prior.range = c(range0, 0.5),
  # P(sigma > 1) = 0.01
  prior.sigma = c(10, 0.01)) 



### ----- 4.3. Defining the inla.stack. It will contain the GF and the covariates --- ####
stk.est <- inla.stack(data    = list(resp = data$resp),
                      A       = list(A.est, 1),
                      effects = list(spatial = 1:spde$n.spde,
                                     data.frame(beta0 = 1,
                                                sim   = data$sim)),
                      tag     = 'est')

### --- 5. Fitting the models and showing posterior distributions --- ####
### ----- 6.1. Fitting --- ####
formula.1 <- resp ~ -1 + beta0 + sim + f(spatial, model = spde)


model.est <- inla(formula.1,
                  data              = inla.stack.data(stk.est), 
                  family            = "binomial" ,
                  control.compute   = list(dic              = TRUE,
                                           cpo              = TRUE, 
                                           waic             = TRUE, 
                                           return.marginals = TRUE), 
                  control.predictor = list(A       = inla.stack.A(stk.est), 
                                           compute = TRUE),
                  control.fixed     = list(mean.intercept = 0,
                                           prec.intercept = 1),
                  #control.inla=list(strategy = "laplace"),
                  num.threads       = 2,
                  verbose           = TRUE)


#saveRDS(model.est, "rds/model_est.rds")
model.est <- readRDS("rds/model_est.rds")
model.est$logfile
summary(model.est)




### ----- 6.2. Posterior distribution of the fixed effects --- ####
model.est$summary.fixed
1 - inla.pmarginal(0, model.est$marginals.fixed$sim)
### ----- 6.3. Posterior distribution hyperpars --- ####
real_range <- 2
plot(model.est$marginals.hyperpar$`Range for spatial`, xlim = c(0,10))
abline(v = real_range)

sigma2w <- 2
plot(model.est$marginals.hyperpar$`Stdev for spatial`, xlim = c(0,5))
abline(v = sqrt(sigma2w))



### ----- 6.4. Posterior distribution of the Gaussian field --- ####
### ------- 6.4.1. Prepare the grid ---- ####
bbox(saudi_bound)
(dxy <- apply(bbox(saudi_bound),1, diff))
(r <- dxy[1]/dxy[2])
m <- 250
proj.grid.mat <- 
  inla.mesh.projector(mesh, 
                      xlim = bbox(saudi_bound)[1,] + c(-1, +1),
                      ylim = bbox(saudi_bound)[2,] + c(-1, +1),
                      dims=c(r, 1)*m)



plot(saudi_bound)
points(proj.grid.mat$lattice$loc, pch  = 20, cex = 0.5)


### --- clean (set NA to the values outside boundary) --- ###
ov <- over(SpatialPoints(proj.grid.mat$lattice$loc, 
                         saudi_bound@proj4string),
           saudi_bound)

### --- check grid points inside the map --- ###
i.map <- is.na(ov)
table(i.map)

### Plot the points where we will predict ###
png("images/points_grid.png", width = 800, height = 700, res = 100)
par(mar=c(0,0,0,0))
plot(saudi_bound)
points(proj.grid.mat$lattice$loc[!i.map,], col="red", cex=0.2)
points(proj.grid.mat$lattice$loc[i.map,], col="blue", cex=0.2)
dev.off()
### --- consider only those inside map --- ###
proj.grid.mat$lattice$loc[i.map, ]

### ------- 6.4.2. Projection of the values of the mean and sd of the spatial effect --- ####
mean.g <- inla.mesh.project(proj.grid.mat, model.est$summary.random$spatial$mean)
sd.g <- inla.mesh.project(proj.grid.mat, model.est$summary.random$spatial$sd)
sd.g[i.map] <- mean.g[i.map] <- NA

image.plot(mean.g)
image.plot(sd.g)

### --- Convert to raster --- ###
mean.g.r <- matrix_to_raster(m = mean.g, proj.grid.mat = proj.grid.mat, boundary = saudi_bound)
sd.g.r <- matrix_to_raster(m = sd.g, proj.grid.mat = proj.grid.mat, boundary = saudi_bound)

writeRaster(mean.g.r, file = "rasters/mean_posterior")


### ------- 6.4.3. Visualize --- ####
png("images/spatial_effect.png", width = 1000, height = 500, res = 90)
par(mfrow=c(1,2))
# image.plot(mean.g)
# image.plot(sd.g)
par(mar=c(0,0,0,6))
plot(saudi_bound)
plot(mean.g.r, 
     col = rev(viridis_pal(option = "B")(200)), 
     add = TRUE,
     legend.width  = 3, 
     legend.shrink = 0.7)

#points(kaust, col = "red")
plot(saudi_bound, add = TRUE)

plot(saudi_bound)
plot(sd.g.r, col = rev(viridis_pal(option = "D")(200)), 
     add = TRUE, 
     legend.width  = 3, 
     legend.shrink = 0.7)
plot(saudi_bound, add = TRUE)
dev.off()

### --- 7. Predictions --- ####
### --- Matrix which link the mesh with coordinates to predict--- ###
#A.pred <- inla.spde.make.A(mesh, loc = proj.grid.mat$lattice$loc[!i.map, ])


i.sim <- is.na(extract(sim, proj.grid.mat$lattice$loc))
A.pred <- inla.spde.make.A(mesh, loc = proj.grid.mat$lattice$loc[!i.sim, ])


### --- Stack to predict --- ###
stk.pred <- inla.stack(data    = list(resp = NA),
                       A       = list(A.pred, 1), 
                       effects = list(spatial=1:spde$n.spde,
                                      data.frame(beta0 = 1, 
                                                 sim = extract(sim, proj.grid.mat$lattice$loc[!i.sim,]))),
                       tag     = 'pred')

stk <- inla.stack(stk.est, stk.pred)


#### --- model --- ###
model.pred <- inla(formula.1, 
                   data = inla.stack.data(stk), 
                   family="binomial",
                   control.predictor = list(A       = inla.stack.A(stk), 
                                            compute = TRUE, 
                                            link    = 1), #link:link is a vector of
                   #length given by the size of the response variable with values 1 if the corresponding
                   #data is missing and NA otherwise
                   control.inla      = list(strategy = "simplified.laplace"), # Strategy
                   control.mode      = list(theta = model.est$mode$theta, 
                                            restart = FALSE), #Mode 
                   control.results   = list(return.marginals.random = FALSE,
                                       return.marginals.predictor  = FALSE), # Avoid some marginals
                   num.threads       = 3,
                   verbose  = TRUE)


idx <- inla.stack.index(stk, 'pred')$data

summary(model.pred$summary.fitted.val$mean[idx])

### --- Organize probabilities into a matrix to visualize --- ###
prob.mean <- prob.sd <- matrix(NA, proj.grid.mat$lattice$dims[1],
                               proj.grid.mat$lattice$dims[2])

prob.mean[!i.sim] <- c(model.pred$summary.fitted.val$mean[idx])
prob.sd[!i.sim] <- c(model.pred$summary.fitted.val$sd[idx])



#### --- plot --- ###
par(mfrow=c(1,2))
image.plot(prob.mean)
image.plot(prob.sd)



### --- Generate the raster --- ###
### --- mean --- ###
prob.mean.r <- matrix_to_raster(prob.mean, proj.grid.mat = proj.grid.mat, boundary = saudi_bound)
prob.sd.r <- matrix_to_raster(prob.sd, proj.grid.mat = proj.grid.mat, boundary = saudi_bound)



png("images/predictive_effect.png", width = 1000, height = 500, res = 90)

par(mfrow=c(1,2))
# image.plot(mean.g)
# image.plot(sd.g)

par(mar=c(0,0,0,6))
plot(saudi_bound)
plot(prob.mean.r, 
     col = rev(viridis_pal(option = "B")(200)), 
     add = TRUE,
     legend.width  = 3, 
     legend.shrink = 0.7)

plot(saudi_bound, add = TRUE)

plot(saudi_bound)
plot(prob.sd.r, col = rev(viridis_pal(option = "D")(200)), 
     add = TRUE, 
     legend.width  = 3, 
     legend.shrink = 0.7)
plot(saudi_bound, add = TRUE)
dev.off()


prob.mean.sg <- as(prob.mean.r, "SpatialGridDataFrame")
plotKML(prob.mean.sg, file.name = "saudi_pred.kml")


plot(prob.mean.sg)
### --- sd --- ###
prob.sd.sg <- as(prob.sd.r, "SpatialGridDataFrame")
plotKML(prob.sd.sg, file.name = "saudi_pred_sd.kml")

